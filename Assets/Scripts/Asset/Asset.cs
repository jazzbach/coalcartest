using System;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

/// <summary>
/// Represents an asset in the level editor. 
/// It provides methods for setting position and rotation
/// </summary>
namespace Shane {
	public class Asset : MonoBehaviour {

		/// <summary>
		/// The asset type
		/// </summary>
		[SerializeField]
		private AssetType _assetType;
		public AssetType AssetType {
			get { return _assetType; }
			set { _assetType = value; }
		}


		[Header("Pivot holding 3D model for transform changes")]
		[Header("Internal dependencies")]
		[SerializeField]
		private Transform _pivot;
		public Transform Pivot => _pivot;

		[Header("XR Simple Intertactable (will assign XR Interaction Manager)")]
		[SerializeField]
		private XRSimpleInteractable _xrSimpleInteractable;
		public XRSimpleInteractable XRSimpleInteractable => _xrSimpleInteractable;

		// --------------------------------------------------------------EVENTS

		/// <summary>
		/// Fired when the position of this object has been set
		///		1st arg: This script's instance
		///		2st arg: The transform this object represents
		///		3rd arg: Was GLOBAL position set ? (TRUE = was global. FALSE = was local)
		/// </summary>
		public Action<Asset, Transform, bool> OnSetPosition;

		/// <summary>
		/// Fired when the rotation of this object has been set
		///		1st arg: This script's instance
		///		2st arg: The transform this object represents
		///		3rd arg: Was GLOBAL rotation set ? (TRUE = was global. FALSE = was local)
		/// </summary>
		public Action<Asset, Transform, bool> OnSetRotation;

		// -------------------------------------------------------UNITY METHODS

		private void Start() {

			// Initialize the XR interaction manager for the interactable
			_xrSimpleInteractable.interactionManager = MainSceneContext.GetInstance().XRInteractionManager;
		}

		// -------------------------------------------------------------METHODS

		// -----------------------------------------------------------------SET

		/// <summary>
		/// Sets the position of this asset
		/// </summary>
		/// <param name="position">The position value</param>
		/// <param name="isGlobal">TRUE: global position. FALSE: local position</param>
		public void SetPosition(Vector3 position, bool isGlobal = true) {

			// Do any animation if needed here

			if (isGlobal) {
				transform.position = position;
				OnSetPosition?.Invoke(this, transform, true);
			} else {
				transform.localPosition = position;
				OnSetPosition?.Invoke(this, transform, false);
			}
		}

		/// <summary>
		/// Sets the rotation of this asset (using a quaterion)
		/// </summary>
		/// <param name="rotation">The rotation value</param>
		/// <param name="isGlobal">TRUE: global rotation. FALSE: local rotation</param>
		public void SetRotation(Quaternion rotation, bool isGlobal = true) {

			// Do any animation if needed here

			if (isGlobal) {
				transform.rotation = rotation;
				OnSetRotation?.Invoke(this, transform, true);
			} else {
				transform.localRotation = rotation;
				OnSetRotation?.Invoke(this, transform, false);
			}
		}

		/// <summary>
		/// Sets the rotation of this asset (using euler values)
		/// </summary>
		/// <param name="rotation">The rotation value</param>
		/// <param name="isGlobal">TRUE: global rotation. FALSE: local rotation</param>
		public void SetRotation(Vector3 rotation, bool isGlobal = true) {
			// Delegate the work to the "Quaternion" method
			SetRotation(Quaternion.Euler(rotation), isGlobal);
		}

		/// <summary>
		/// Adds a rotation value to the current rotation (world space)
		/// </summary>
		/// <param name="deltaDegrees"></param>
		public void AddRotation(Vector3 deltaDegrees) {

			// Do any animation if needed here

			transform.Rotate(deltaDegrees, Space.World);
			OnSetRotation?.Invoke(this, transform, true);
		}

		// -----------------------------------------------------------------GET

		/// <summary>
		/// Gets the position value of this object
		/// </summary>
		/// <param name="isGlobal">TRUE: returns global position: FALSE: Returns local position</param>
		/// <returns></returns>
		public Vector3 GetPosition(bool isGlobal = true) {
			if (isGlobal) {
				return transform.position;
			} else {
				return transform.localPosition;
			}
		}

		/// <summary>
		/// Gets the rotation value of this object
		/// </summary>
		/// <param name="isGlobal">TRUE: returns global rotation: FALSE: Returns local rotation</param>
		/// <returns></returns>
		public Quaternion GetRotation(bool isGlobal = true) {
			if (isGlobal) {
				return transform.rotation;
			} else {
				return transform.localRotation;
			}
		}

		// ---------------------------------------------------CREATE ASSET POCO

		public AssetPOCO GetPOCO() {
			return new AssetPOCO() {
				assetType = AssetType,
				position = GetPosition(),
				rotation = GetRotation().eulerAngles
			};
		}

		// ---------------------------------------------OVERRIDEN (FROM OBJECT)

		public override bool Equals(object obj) {
			return obj is Asset asset
				&& base.Equals(obj);
		}

		public override int GetHashCode() {
			return 624022166 + base.GetHashCode();
		}
	}
}