using System;

/// <summary>
/// Represents the type of the asset. Used by "Asset"
/// </summary>
namespace Shane {
	[Serializable]
	public enum AssetType {
		A, B
	}
}