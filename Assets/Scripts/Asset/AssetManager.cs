using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Service that handles the lifecycle of the assets in the scene
/// </summary>
namespace Shane {

	[Serializable]
	public class KeyValuePair {
		public AssetType key;
		public Asset val;
	}

	public class AssetManager : MonoBehaviour {

		[SerializeField]
		private List<KeyValuePair> _assetPrefabs;

		[Header("The amount of rotation to be added (or substracted) to an asset")]
		[SerializeField]
		private float _rotationDelta;
		public float RotationDelta => _rotationDelta;

		// ---------------------------------------------------PRIVATE VARIABLES

		// Quick lookup. Element uniqueness (using Asset's overriden methods
		// "Equals" and "GetHashCode")
		private HashSet<Asset> _assetSet = new HashSet<Asset>();
		public HashSet<Asset> AssetSet => _assetSet;

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Returns all the assets contained in the set as an object that contains
		/// a list of the pocos' data
		/// </summary>
		/// <param name="onGetAllAssetPocos">The action to be executed at the end of the method (if any)</param>
		/// <returns>All the assets contained in the set as an object that contains
		/// a list of the pocos' data</returns>
		public AssetPOCOList GetAllAssetPOCOs(Action<AssetManager, AssetPOCOList> onGetAllAssetPocos = null) {

			// Create a new object
			AssetPOCOList t_assetPocoList = new AssetPOCOList();

			List<AssetPOCO> t_pocoList = new List<AssetPOCO>(_assetSet.Count);
			foreach (Asset asset in _assetSet) {
				t_pocoList.Add(asset.GetPOCO());
			}

			t_assetPocoList.assetPOCOList = t_pocoList;

			// Execute the argument action in case is not null
			onGetAllAssetPocos?.Invoke(this, t_assetPocoList);

			return t_assetPocoList;
		}

		/// <summary>
		/// Creates an asset with the given parameters:
		/// </summary>
		/// <param name="position">The new position of the asset</param>
		/// <param name="rotation">The new rotation of the asset</param>
		/// <param name="type">The type of the asset</param>
		/// <param name="onCreateAsset">The action to be executed at the end of the method (if any)</param>
		/// <returns>The created asset</returns>
		public Asset CreateAsset(Vector3 position, Quaternion rotation, AssetType type,
				Action<AssetManager, Asset> onCreateAsset = null) {

			// Create an asset depending on the type of asset
			Asset t_assetPrefab = _GetAssetPrefabFromList(type);
			Asset t_asset = Instantiate(t_assetPrefab, position, rotation, this.gameObject.transform);

			// Set the type of the asset
			t_asset.AssetType = type;

			// Add the asset into the set
			_assetSet.Add(t_asset);

			// Execute the argument action in case is not null
			onCreateAsset?.Invoke(this, t_asset);

			return t_asset;

		}

		/// <summary>
		/// Creates all the assets represented by the given list
		/// </summary>
		/// <param name="pocoList">The list of all the asset data</param>
		/// <param name="onCreateAllAssets">The action to be executed at the end of the method (if any)</param>
		public void CreateAllAssets(AssetPOCOList pocoList, Action<AssetManager> onCreateAllAssets = null) {

			// Clears all assets from the scene
			ClearAssetSet();

			// Create all assets from the list
			foreach (AssetPOCO poco in pocoList.assetPOCOList) {
				CreateAsset(poco.position, Quaternion.Euler(poco.rotation), poco.assetType);
			}

			// Execute the argument action in case is not null
			onCreateAllAssets?.Invoke(this);
		}

		/// <summary>
		/// Deletes the given asset from the scene and from the internal list
		/// </summary>
		/// <param name="asset">The asset to be deleted</param>
		/// <param name="onDeleteAsset">The action to be executed at the end of the method (if any)</param>
		/// <returns></returns>
		public bool DeleteAsset(Asset asset, Action<AssetManager> onDeleteAsset = null) {
			if (_assetSet.Contains(asset) == false) return false;

			// Remove asset from the list
			_assetSet.Remove(asset);

			// Destroy game object
			Destroy(asset.gameObject);

			// Execute the argument action in case is not null
			onDeleteAsset?.Invoke(this);

			return true;
		}

		/// <summary>
		/// Adds the given rotation to the given asset.
		/// </summary>
		/// <param name="asset">The asset to be rotated</param>
		/// <param name="deltaDegrees">The amount of degrees to add to its current rotation</param>
		/// <param name="onAddRotation">The action to be executed at the end of the method (if any)</param>
		public void AddRotationToAsset(Asset asset, Vector3 deltaDegrees, Action<AssetManager, Asset> onAddRotation = null) {
			asset.AddRotation(deltaDegrees);
			onAddRotation?.Invoke(this, asset);
		}

		public void AddRotationToAsset(Asset asset, bool isYClockwise, Action<AssetManager, Asset> onAddRotation = null) {
			Vector3 t_rotationVector = Vector3.up;
			t_rotationVector *= isYClockwise ? _rotationDelta : -_rotationDelta;

			AddRotationToAsset(asset, t_rotationVector, onAddRotation);
		}

		/// <summary>
		/// Destroys all Asset objects from the scene and clears the internal 
		/// collection
		/// </summary>
		/// <param name="onClearAssetSet">The action to be executed at the end of the method (if any)</param>
		public void ClearAssetSet(Action<AssetManager> onClearAssetSet = null) {

			// Destroy all Asset objects in scene
			foreach (Asset asset in _assetSet) Destroy(asset.gameObject);

			// Clear the list
			_assetSet.Clear();

			// Execute the argument action in case is not null
			onClearAssetSet?.Invoke(this);
		}

		// -----------------------------------------------------PRIVATE METHODS

		/// <summary>
		/// Gets the corresponding value from the key-value list of possible 
		/// prefabs, using the AssetType
		/// </summary>
		/// <param name="assetType">The type of Asset type</param>
		/// <returns></returns>
		private Asset _GetAssetPrefabFromList(AssetType assetType) {
			return _assetPrefabs.First(e => e.key.Equals(assetType)).val;
		}
	}
}