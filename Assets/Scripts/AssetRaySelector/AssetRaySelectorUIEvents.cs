using System;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

/// <summary>
/// Performs asset selection and maintains a reference of the currently selected
/// asset.
/// </summary>
namespace Shane {
	public class AssetRaySelectorUIEvents : MonoBehaviour {

		// ---------------------------------------------------PUBLIC PROPERTIES

		// Holds a reference to the currently selected asset. If no assets are
		// selected, NULL will be set to this reference
		private Asset _selectedAsset = null;
		public Asset SelectedAsset => _selectedAsset;

		// --------------------------------------------------------------EVENTS

		/// <summary>
		/// Fired when there is a hover event change (either on hover enter or
		/// on hover exit).
		/// To check if there was an object selected, a simple check on 
		/// "SelectedAsset" to null can be performed
		/// </summary>
		public Action<AssetRaySelectorUIEvents> OnHoverEventFired;

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Event that will be fired when hovering an XR interactable
		/// </summary>
		/// <param name="args">event args</param>
		public void OnHoverEnter(HoverEnterEventArgs args) {

			_selectedAsset = args.interactableObject.colliders[0].GetComponent<Asset>();
			//_context.AssetManager.AddRotationToAsset(a, true);
			//_context.AssetManager.DeleteAsset(a);

			OnHoverEventFired?.Invoke(this);

		}

		/// <summary>
		/// Event that will be fired when exiting hovering an XR interactable
		/// </summary>
		/// <param name="args">event args</param>
		public void OnHoverExit(HoverExitEventArgs args) {

			_selectedAsset = null;
			OnHoverEventFired?.Invoke(this);
		}
	}
}