using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

/// <summary>
/// Service that simply encapsulates all MonoBehaviour objects meaningful to
/// a specific scene. Can be used as a service locator (like "yellow pages" to
/// search for objects)
/// </summary>
namespace Shane {
	public class MainSceneContext : MonoBehaviour {

		// Private Singleton (can only bet accessed via "GetInstance" method)
		private static MainSceneContext CONTEXT_MAIN_SCENE;

		// --------------------------------------------------UNITY DEPENDENCIES

		[SerializeField]
		private XRInteractionManager _xrInteractionManager;
		public XRInteractionManager XRInteractionManager => _xrInteractionManager;

		[SerializeField]
		private RayColliderPositionSnapper _rayColliderPositionSnapper;
		public RayColliderPositionSnapper RayColliderPositionSnapper => _rayColliderPositionSnapper;

		[SerializeField]
		private AssetManager _assetManager;
		public AssetManager AssetManager => _assetManager;

		[SerializeField]
		private AssetRaySelectorUIEvents _assetRaySelectorUIEvents;
		public AssetRaySelectorUIEvents AssetRaySelectorUIEvents => _assetRaySelectorUIEvents;

		[SerializeField]
		private GameCanvas _gameCanvas;
		public GameCanvas GameCanvas => _gameCanvas;

		// -------------------------------------------------------UNITY METHODS

		public void Awake() {
			// Assign singleton
			if (CONTEXT_MAIN_SCENE == null) {
				CONTEXT_MAIN_SCENE = this;
			}
		}

		// ------------------------------------------------------PUBLIC METHODS

		public static MainSceneContext GetInstance() {
			return CONTEXT_MAIN_SCENE;
		}
	}
}