using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Registers all input events from the new input system and performs actions
/// on the objects
/// </summary>
namespace Shane {
	public class GameInputActions : MonoBehaviour {

		// ----------------------------------------------------UNITY PROPERTIES

		[Header("The context containing the AssetRayUIEvents to check for asset selection")]
		[SerializeField]
		private MainSceneContext _context;
		public MainSceneContext Context => _context;

		// ---------------------------------------------------PRIVATE VARIABLES
		/// <summary>
		/// Object dealing with all the user input
		/// </summary>
		private InputActions _inputActions;

		// CACHED OBJECTS (from Context)
		private AssetManager _assetManager;
		private AssetRaySelectorUIEvents _assetRaySelectorUIEvents;
		private RayColliderPositionSnapper _rayColliderPositionSnapper;

		// -------------------------------------------------------UNITY METHODS

		private void Awake() {

			// Create a new ionput action object
			_inputActions = new InputActions();
		}

		private void OnEnable() {
			_inputActions.Enable();
		}

		private void OnDisable() {
			_inputActions.Disable();
		}

		private void Start() {

			// Cache objects
			_assetManager = _context.AssetManager;
			_assetRaySelectorUIEvents = _context.AssetRaySelectorUIEvents;
			_rayColliderPositionSnapper = _context.RayColliderPositionSnapper;

			// Register events

			// LEFT HAND


			/*
			NOTE: There are some repeated code down here. It can be refactored
			to have common methods per event registration and thus, way less 
			repetition
			*/

			// CREATE ASSET A
			_inputActions.ShaneIO.PlaceAssetA.performed += callbackContext => {
				if (_rayColliderPositionSnapper.IsMarkerPositionValid) {

					// Get marker position
					Vector3 t_markerPos = _rayColliderPositionSnapper.Marker.position;

					// Create the asset in the given position
					_assetManager.CreateAsset(t_markerPos, Quaternion.identity, AssetType.A);
				}
			};

			// CREATE ASSET B
			_inputActions.ShaneIO.PlaceAssetB.performed += callbackContext => {
				if (_rayColliderPositionSnapper.IsMarkerPositionValid) {

					// Get marker position
					Vector3 t_markerPos = _rayColliderPositionSnapper.Marker.position;

					// Create the asset in the given position
					_assetManager.CreateAsset(t_markerPos, Quaternion.identity, AssetType.B);
				}
			};

			// RIGHT HAND

			// ROTATE ASSET L
			_inputActions.ShaneIO.RotateAssetL.performed += callbackContext => {
				if (_assetRaySelectorUIEvents.SelectedAsset != null) {
					_assetManager.AddRotationToAsset(
						_assetRaySelectorUIEvents.SelectedAsset, 
						true); // Rotate clockwise
				}
			};

			// ROTATE ASSET R
			_inputActions.ShaneIO.RotateAssetR.performed += callbackContext => {
				if (_assetRaySelectorUIEvents.SelectedAsset != null) {
					_assetManager.AddRotationToAsset(
						_assetRaySelectorUIEvents.SelectedAsset, 
						false); // Rotate anti clockwise
				}
			};

			// DELETE ASSET
			_inputActions.ShaneIO.DeleteAsset.performed += callbackContext => {
				if (_assetRaySelectorUIEvents.SelectedAsset != null) {
					_assetManager.DeleteAsset(_assetRaySelectorUIEvents.SelectedAsset);
				}
			};
		}
	}
}