using System.Collections.Generic;
using System.IO;

/// <summary>
/// Provides services for loading and saving Asset objects from and into files 
/// (json files)
/// </summary>
namespace Shane {
	public static class IOAssetLoader{
		public const string FILENAME = "assets.json";

		/// <summary>
		/// Tries to load from the project folder the json file named "FILENAME". 
		/// The resulting config file
		/// will be transformed as an object of type "COnfigPoco" and returned
		/// as an "out" parameter named "configObj".
		/// 
		/// If the file doesn't exist, a new file will be created and the object
		/// returned as described above.
		/// 
		/// If the whole process is successful, True will be returned.
		/// If at any given moment there was an error, false will be returned
		/// instead.
		/// 
		/// </summary>
		/// <param name="configObj">The "return" object containing all the 
		/// configuration</param>
		/// <returns>True: If the process was successful, False otherwise</returns>
		public static bool LoadFile(out AssetPOCOList resultAssetPocoList) {

			// Checks if the file exists. If not, return immediately
			string t_configFilePath = _GetAppPath(FILENAME);
			if (File.Exists(t_configFilePath) == false) {
				resultAssetPocoList = default;
				return false;
			}

			// Read the file and store its contents as a string
			// (if function returns true, the file read was successful)
			if (IOStringFiles.Read(t_configFilePath, out string t_jsonConfigString)) {
				// Deserialize the json into a class (a "poco" class)
				//
				// "IOJsons.JsonToObj()" method will return true if the process was
				// successful. False otherwise
				// Note that the "out" obj value will be assigned here
				return IOJsons.JsonToObj<AssetPOCOList>(t_jsonConfigString, out resultAssetPocoList);
			}

			resultAssetPocoList = default;
			return false;
		}

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Creates a json file representing an AssetPocoList class that will be 
		/// saved inside the project folder. The file name will be given by
		/// the "FILENAME" static variable of this class.
		/// 
		/// If the whole process is successful, True will be returned.
		/// If at any given moment there was an error, false will be returned
		/// instead
		/// 
		/// If the file already exists, this will overwrite it.
		/// </summary>
		/// <returns>True: If the whole process was successful. 
		/// False otherwise</returns>
		public static bool CreateFile(AssetPOCOList assetPocoList) {

			// Convert the "pocoList" into a json string
			// (if function returns true, the parse was successful)
			if (IOJsons.ObjToJson(assetPocoList, out string jsonString)) {

				// Save the "poco" json string into a file
				// (filename Will be "CONFIG_FILENAME" and will be saved inside
				// the project folder)
				// "IOStringFiles.Write()" method will return true if the process was
				// successful. False otherwise

				return IOStringFiles.Write(
					_GetAppPath(FILENAME), // path to where the file will be saved
					jsonString);			// json string
			}

			return false;
		}

		// -----------------------------------------------------PRIVATE METHODS
		/// <summary>
		/// Return the absolute application path where the app is being run, plus
		/// any given filename (if not null or empty string).
		/// 
		/// Example: If "filename" = myFile.txt
		/// and if run from the Unity editor (and the path of the project is:
		/// c:\path\to\project), the full returning string path will be:
		///		c:\path\to\project\myFile.txt
		///		
		/// If run from a build (exe), this will return the exe folder path.
		/// plus the "filename" at the end.
		/// 
		/// If "filename" is null or an empty string, it'll only return the
		/// project path
		/// </summary>
		/// <param name="filename">Any filename that needs to be attached at 
		/// the end of the project path. Can be null or empty string</param>
		/// <returns>the absolute application path where the app is being run, plus
		/// any given filename (if not null or empty string).</returns>
		private static string _GetAppPath(string filename) {
			// NOTE: This method should be inside a static Utility class 
			if (string.IsNullOrEmpty(filename))
				return Directory.GetCurrentDirectory();
			else
				return Path.Combine(Directory.GetCurrentDirectory(), filename);
		}
	}
}