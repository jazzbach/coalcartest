using System;
using UnityEngine;

/// <summary>
/// Class that represents the persistent state of an Asset object
/// </summary>
namespace Shane {
	
	[Serializable]
	public class AssetPOCO {

		public AssetType assetType;
		public Vector3 position;
		public Vector3 rotation;
	}
}