using System;
using UnityEngine;

/// <summary>
/// Utility class that helps in the serialization and deserialization 
/// process of objects.
/// </summary>
namespace Shane {
	
	public static class IOJsons {
		/// <summary>
		/// Deserializes (inflates) an object from a json string.
		/// 
		/// If there are errors in the process, false will be returned
		/// True will be returned when no errors occurred.
		/// </summary>
		/// <typeparam name="T">The type of the object to be deserialized</typeparam>
		/// <param name="jsonString">The json string to be deserialized into an object</param>
		/// <param name="obj">The resulting deserialized object of type T</param>
		/// <returns>True if the process was successful. False otherwise</returns>
		public static bool JsonToObj<T>(string jsonString, out T obj) {
			try {
				obj = JsonUtility.FromJson<T>(jsonString);
			} catch (Exception) {
				obj = default;
				// Should return an object with the status of the error instead
				// Not doing it for the sake of simplicity
				return false;
			}
			return true;
		}

		/// <summary>
		/// Serializes (deflates) an object into a json string.
		/// 
		/// If there are errors in the process, false will be returned
		/// True will be returned when no errors occurred.
		/// 
		/// 
		/// </summary>
		/// <param name="obj">The object to be serialized</param>
		/// <param name="jsonString">The string that will contain the 
		/// resulting json representation of the object to be serialized</param>
		/// <returns>True if the process was successful. False otherwise</returns>
		public static bool ObjToJson(object obj, out string jsonString) {
			try {
				jsonString = JsonUtility.ToJson(obj, true); // true: pretty print
			} catch (Exception) {
				jsonString = default;
				// Should return an object with the status of the error instead
				// Not doing it for the sake of simplicity
				return false;
			}
			return true;
		}
	}
}