using System;
using System.Collections.Generic;

/// <summary>
/// Class that represents a collection of AssetPOCOs
/// 
/// Used mainly to serialize and deserialize data
/// </summary>
namespace Shane {
	[Serializable]
	public class AssetPOCOList {
		public List<AssetPOCO> assetPOCOList = new List<AssetPOCO>();
	}
}