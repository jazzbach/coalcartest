using System;
using System.IO;

/// <summary>
/// Utility class that helps in the loading and saving of string 
/// from and to a file respectively
/// </summary>
namespace Shane {
	public static class IOStringFiles {

		/// <summary>
		/// Writes the given string "s" into a file in the given "path".
		/// If the file already exists, it will be overwritten.
		/// 
		/// If there are errors when saving the file, false will be returned
		/// True will be returned when no errors occurred.
		/// 
		/// The encoding in which the text will be saved will be UTF-8
		/// 
		/// No exceptions will be thrown from this method.
		/// </summary>
		/// <param name="path">The path where the file will be saved</param>
		/// <param name="s">The string to be saved</param>
		/// <returns>True: If the string was saved in the given path succesfully. 
		/// False otherwise</returns>
		public static bool Write(string path, string s) {
			try {
				File.WriteAllText(path, s, System.Text.Encoding.UTF8);
			} catch (Exception) {
				// Should return an object with the status of the error instead
				// Not doing it for the sake of simplicity
				return false;
			}
			return true;
		}

		/// <summary>
		/// Reads the string contents (if any) of the given file "path"
		/// 
		/// If there are errors when reading the file, false will be returned
		/// True will be returned when no errors occurred.
		/// 
		/// The encoding in which the text will be read will be UTF-8
		/// 
		/// No exceptions will be thrown from this method.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="s"></param>
		/// <returns></returns>
		public static bool Read(string path, out string s) {
			try {
				s = File.ReadAllText(path, System.Text.Encoding.UTF8);
			} catch (Exception) {
				s = default;

				// Should return an object with the status of the error instead
				// Not doing it for the sake of simplicity
				return false;
			}
			return true;
		}
	}
}