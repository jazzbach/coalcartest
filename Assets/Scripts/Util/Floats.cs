/// <summary>
/// Provides utility methods for floats
/// </summary>
namespace Shane {
	public static class Floats {

		/// <summary>
		/// Returns true if value is in between minValue and maxValue
		/// </summary>
		/// <param name="value"></param>
		/// <param name="minValue"></param>
		/// <param name="maxValue"></param>
		/// <returns></returns>
		public static bool Between(float value, float minValue, float maxValue) {
			return (value > minValue && value < maxValue);
		}

		/// <summary>
		/// Returns true if "value" is in between the negative and positive
		/// values of "negPosAbsValue", which should be provided positive. 
		/// 
		/// If "negPosAbsValue" is provided as negative, its positive value
		/// will be internally used.
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="negPosAbsValue"></param>
		/// <returns></returns>
		public static bool Between(float value, float negPosAbsValue) {

			// In case ngePosAbsValue is negative, make it positive
			if (negPosAbsValue < 0F) negPosAbsValue *= -1F;

			return Between(value, -negPosAbsValue, negPosAbsValue);
		}
	}
}