using UnityEngine;

/// <summary>
/// Provides services for transforming a Vector3 position into a snapped
/// position (like a grid snap)
/// </summary>
namespace Shane {
	public class PositionSnapper : MonoBehaviour {

		// ----------------------------------------------------UNITY PROPERTIES

		/// <summary>
		/// Provides an increment for the snap.
		/// Example (on X axis only): If a value of 1.23 is given and the 
		/// X value of "_incrementXZ" is 0.5, then 1.23 will "snap" to the
		/// next lowest value, which will be 1:
		/// 
		///                  1.23 (snaps to 1)
		///                  |
		/// |......|......|......|
		/// 0     0.5     1     1.5
		/// </summary>
		[Header("Snap increment")]
		[SerializeField]
		private Vector3 _incrementXZ;
		public Vector3 IncrementXZ => _incrementXZ;

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Transforms the given "inputPosition" into a snapped value
		/// </summary>
		/// <param name="inputPosition"></param>
		/// <returns></returns>
		public Vector3 SnapXZ(Vector3 inputPosition) {

			/*
			The process of positioning starts with offsetting half of the 
			"increment" value. This is because currently, the snap is 
			calculated using the "Lower Right" corner value, but instead, we 
			need to use the "Center" of the span.
			*/
			inputPosition.x += (_incrementXZ.x * 0.5F);
			inputPosition.z += (_incrementXZ.z * 0.5F);

			/*
			Once the position has been offset, we divide the global positon by the increment in X and Z
			This will "normalize" the current value by "_increment". However,
			this will make a step, always a value of one and not the real
			"increment value" (for example, if the increment is 0.5 and the input position 3,
			dividing 3 by 0.5 will result in 6, but the position is NOT at 6, is at 3)
			. So, simply multiplying that "normalized" value by the increment will solve the issue.
			*/
			inputPosition.x = Mathf.Floor(inputPosition.x / _incrementXZ.x) * _incrementXZ.x;
			inputPosition.z = Mathf.Floor(inputPosition.z / _incrementXZ.z) * _incrementXZ.z;


			return inputPosition;
		}
	}
}