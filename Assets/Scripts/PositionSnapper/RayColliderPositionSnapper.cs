using System;
using UnityEngine;

/// <summary>
/// Performs position snaps (like grid snaps) when rays hit the given collider
/// </summary>
namespace Shane {
	public class RayColliderPositionSnapper : MonoBehaviour {

		// ----------------------------------------------------UNITY PROPERTIES

		[Header("Transform used to direct rays")]
		[SerializeField]
		private Transform _rayTransform;
		public Transform RayTransform => _rayTransform;

		[Header("Layer mask to be used to register ray hits (to show position sphere)")]
		[SerializeField]
		private LayerMask _layerMask;
		public LayerMask LayerMask => _layerMask;

		[Header("Raycast collider to perform position snaps on")]
		[SerializeField]
		private Collider _collider;
		public Collider Collider => _collider;

		[Header("Raycast distance")]
		[SerializeField]
		private float _rayDistance;
		public float RayDistance => _rayDistance;

		[Header("Snapping will only occur within +-MaxAbsValueXZ")]
		[SerializeField]
		private Vector3 _maxAbsValueXZ;
		public Vector3 MaxAbsValueXZ => _maxAbsValueXZ;

		[Header("Script that will perform the snapping logic")]
		[Header("Internal dependencies:")]
		[SerializeField]
		private PositionSnapper _positionSnapper;
		public PositionSnapper PositionSnapper => _positionSnapper;

		[Header("Position marker (to mark snapped position)")]
		[SerializeField]
		private Transform _marker;
		public Transform Marker => _marker;

		// ---------------------------------------------------PUBLIC PROPERTIES

		/// <summary>
		/// Returns true if the current position of the marker is valid 
		/// (to place objects)
		/// </summary>
		private bool _isMarkerPositionValid;
		public bool IsMarkerPositionValid => _isMarkerPositionValid;

		// ---------------------------------------------------PRIVATE VARIABLES

		/// <summary>
		/// Stores the previous snapped position. This is just to avoid
		/// executing the action event inside "RayCollide" every frame. Only
		/// when there was a value change between "_previousSnappedPosition" and
		/// "snappedPosition" (of current frame)
		/// </summary>
		private Vector3 _previousSnappedPosition = Vector3.zero;

		// -------------------------------------------------------UNITY METHODS

		private void Start() {

			// Disable the marker (untiul there's a raycast hit)
			SetEnableMarker(false);
		}

		private void OnDrawGizmos() {
			
			// Draw snapping "area of effect" limits
			Gizmos.color = Color.blue;
			Gizmos.DrawWireCube(Vector3.zero, new Vector3(_maxAbsValueXZ.x * 2, 0.3F, _maxAbsValueXZ.z * 2));
		}

		private void Update() {

			// Will perform the raycasting to the floor every update
			// To paint the positional sphere
			RaycastHit hit;
			Ray ray = new Ray(_rayTransform.position, _rayTransform.forward);

			RayCollide(ray, out hit, _layerMask, out Vector3 snappedPosition);
		}

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ray"></param>
		/// <param name="rayHit"></param>
		/// <param name="layerMask"></param>
		/// <param name="snappedPosition"></param>
		/// <param name="onRayHit"></param>
		/// <returns></returns>
		public bool RayCollide(Ray ray, out RaycastHit rayHit, LayerMask layerMask, out Vector3 snappedPosition, Action onRayHit = null) {

			// Initialize out values
			snappedPosition = Vector3.zero;

			// Fire a ray to the collider
			bool _wasHit = Physics.Raycast(ray, out rayHit, _rayDistance, layerMask);

			// If there was a hit
			if (_wasHit) {

				// Snap the value of the collision position
				snappedPosition = _positionSnapper.SnapXZ(rayHit.point);

				// Check if the snapped value is within the valid XZ limits
				_wasHit = Floats.Between(snappedPosition.x, _maxAbsValueXZ.x) && Floats.Between(snappedPosition.z, _maxAbsValueXZ.z);

				// Move the marker to the position
				_marker.position = snappedPosition;

				// Execute the given action (if any), only if previous position
				// is difference from the current one and if there was a hit
				// (within the valid XZ limits)
				if (_previousSnappedPosition != snappedPosition && _wasHit) onRayHit?.Invoke();

				// Set "_previousSnappedPosition"
				_previousSnappedPosition = snappedPosition;
			}

			// Set the marker validity bool flag
			_isMarkerPositionValid = _wasHit;

			// Show or hide the marker
			SetEnableMarker(_wasHit);

			return _wasHit;
		}

		/// <summary>
		/// Enables or disables the marker (red sphere)
		/// </summary>
		/// <param name="isEnabled"></param>
		private void SetEnableMarker(bool isEnabled) {
			_marker.gameObject.SetActive(isEnabled);
		}
	}
}