using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Represents all the UI elements and events that will delegate all the work
/// to the respective methods
/// </summary>
namespace Shane {
	public class GameCanvas : MonoBehaviour {

		// ----------------------------------------------------UNITY PROPERTIES

		/// <summary>
		/// Context where all the dependencies will be taken
		/// </summary>
		[SerializeField]
		private MainSceneContext _context;
		public MainSceneContext Context => _context;

		[SerializeField]
		private Button _loadFromFileButton;
		public Button LoadFromFileButton => _loadFromFileButton;

		[SerializeField]
		private Button _saveToFileButton;
		public Button SaveToFileButton => _saveToFileButton;

		[SerializeField]
		private Button _clearSceneButton;
		public Button ClearSceneButton => _clearSceneButton;

		[SerializeField]
		private TMP_Text _messagesText;
		public TMP_Text MessagesText => _messagesText;

		// -----------------------------------------------------CACHED ELEMENTS

		// To delete all the objects from the scene
		private AssetManager _assetManager;

		// -------------------------------------------------------UNITY METHODS

		private void Start() {
			_assetManager = _context.AssetManager;
		}

		// ------------------------------------------BUTTON METHODS (UI EVENTS)

		public void OnLoadFromFileButton() {
			_ClearMessageText();
			if (IOAssetLoader.LoadFile(out AssetPOCOList assetList)) {
				_assetManager.CreateAllAssets(assetList);

				_messagesText.color = Color.green;
				_messagesText.text = "Assets created successfully !";
			} else {
				_messagesText.color = Color.red;
				_messagesText.text = $"\"{IOAssetLoader.FILENAME}\" file doesn't exist in project";
			}
		}

		public void OnSaveToFileButton() {
			_ClearMessageText();

			// Creates the file with all the assets in scene
			if (IOAssetLoader.CreateFile(_assetManager.GetAllAssetPOCOs())) {
				_messagesText.color = Color.green;
				_messagesText.text = $"\"{IOAssetLoader.FILENAME}\" file created successfully !";
			} else {
				_messagesText.color = Color.red;
				_messagesText.text = $"\"{IOAssetLoader.FILENAME}\" file couldn't be created";
			};
		}

		public void OnClearSceneButton() {
			_ClearMessageText();

			// Deletes all objects from scene
			_assetManager.ClearAssetSet();

			_messagesText.color = Color.blue;
			_messagesText.text = "Objects cleared from scene successfully";
		}

		// -----------------------------------------------------PRIVATE METHODS

		/// <summary>
		/// Clears the message text (assigns the empty string)
		/// </summary>
		private void _ClearMessageText() {
			_messagesText.text = string.Empty;
		}
	}
}