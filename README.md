# CoalcarTest

Coalcar test (Shane)

Made by Bruno Arturo Costa Hernandez
chilljazzbach@gmail.com

Gitlab link:
https://gitlab.com/jazzbach/coalcartest.git

## Running the project from within Unity

In order to run the code, Unity 2020.3.25f1 (LTS) should be used.

The project has all the packages loaded correctly, which were:

XR Plugin Management
XR Interaction Toolkit
Input System

## Important note on saving files

Saved files are stored as a "assets.json" file. This file is created in the root folder of the project everytime a scene is saved (when clicking in the "Save to file" In-Game UI button).

Files can be overwriten if you click on the "Save to file" button again. Be careful :D

If you'd like to load a saved "assets.json" file:

- Place the "assets.json" in the root folder of the project (if there isn't one yet)
- Start the application with your VR headset
- Click on the "Load from file" In-Game UI button
